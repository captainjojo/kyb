import { AuthenticationContext, adalFetch, withAdalLogin } from 'react-adal';

// Configuration d'ADAL
export const adalConfig = {
    tenant: "612990a4-febd-41ab-9cd0-6fe884cc8bee", // id ou url du tenant Azure
    clientId: "64340310-4ac3-4892-b27c-ee3aaa662005", // id de l'application créé ci-dessus
    endpoints: {
        api: "https://login.microsoftonline.com/612990a4-febd-41ab-9cd0-6fe884cc8bee/oauth2/v2.0/authorize"
    },
    redirectUri: "http://localhost:3000/signin-oidc", // url de redirection
    postLogoutRedirectUri: "http://localhost:3000", // url appelée lors de la déconnexion
    cacheLocation: 'localStorage'
};

export const authContext = new AuthenticationContext(adalConfig);

// Méthode de récupération du token
export const getToken = () => {
    var cachedToken = authContext.getCachedToken(authContext.config.clientId);
    if (cachedToken === undefined) {
        authContext.acquireToken(authContext.config.clientId);
    }
    else {
        return cachedToken;
    }
};

// Méthode permettant d'exécuter un appel via la librairie ADAL
export const adalApiFetch = (fetch, url, options) =>
    adalFetch(authContext, adalConfig.endpoints.api, fetch, url, options);

export const withAdalLoginApi = withAdalLogin(authContext, adalConfig.endpoints.api);
