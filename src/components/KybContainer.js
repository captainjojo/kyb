import React, { useEffect } from 'react';
import Kyb from './kyb';
import Grid from '@material-ui/core/Grid';
import { makeStyles } from '@material-ui/core/styles';
import Pdfs from './pdfs';
import { Paper } from '@material-ui/core';

const useStyles = makeStyles(theme => ({
    root: {
        flexGrow: 1,
    },
    paper: {
        padding: theme.spacing(2),
        textAlign: 'left',
        color: theme.palette.text.secondary,
    },
}));


const KybContainer = ({ match }) => {
    const classes = useStyles();

    useEffect(() => {
        console.log(match);
    });

    return (

        <Grid container className={classes.root} spacing={6} lg={12}>
            <Grid item lg={5}>
                <Paper className={classes.paper}> <Kyb id={match.params.id}></Kyb></Paper>
            </Grid>
            <Grid item lg={7}>
                <Paper className={classes.paper}> <Pdfs id={match.params.id}></Pdfs></Paper>
            </Grid>

        </Grid>
    );
};


export default KybContainer;