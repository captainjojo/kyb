import React, { useState, useEffect } from 'react';
import Axios from 'axios';
import { FormControl, Select, MenuItem } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { getToken } from '../adalConfig';

const useStyles = makeStyles(theme => ({
    formControl: {
        margin: theme.spacing(1),
        minWidth: 120,
    },
}));

const PdfType = ({ id, file }) => {
    const [typeFile, setTypeFile] = useState('');
    const classes = useStyles();

    useEffect(() => {
        setTypeFile(file.type);
    }, [file])

    const handleChange = (path, event) => {
        const update = async (type, status) => {
            const token = getToken();
            await Axios.put(
                `${process.env.REACT_APP_API_URL}/kybdocs/update`, { path, type }, {
                headers: {
                    "Authorization": `Bearer ${token}`,
                }
            }
            );
        };

        update(event.target.value, path);
        setTypeFile(event.target.value);
    }


    return (
        <FormControl className={classes.formControl}>
            <Select
                value={typeFile}
                onChange={handleChange.bind(this, 'upload/kybdocs/' + id + '/' + file.filename)}
                inputProps={{
                    name: `status${id}`,
                    id: `status${id}`,
                }}
            >
                <MenuItem value="None">
                    <em>None</em>
                </MenuItem>
                <MenuItem value='KBIS'>KBIS</MenuItem>
                <MenuItem value='ID'>Identité</MenuItem>
                <MenuItem value='Justificatif'>Justificatif de domicile</MenuItem>
                <MenuItem value='Statut'>Statut</MenuItem>
                <MenuItem value='Demande de libération'>Demande de libération</MenuItem>
                <MenuItem value="Avis d'opérer">Avis d'opérer</MenuItem>
            </Select>
        </FormControl>
    );
}


export default PdfType;