import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Axios from 'axios';
import { getToken } from '../adalConfig';

const useStyles = makeStyles(theme => ({
    container: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    textField: {
        marginLeft: theme.spacing(1),
        marginRight: theme.spacing(1),
        width: 450,
    },
    dense: {
        marginTop: 19,
    },
    menu: {
        width: 200,
    },
}));

const Company = ({ company }) => {
    const classes = useStyles();
    const [values, setValues] = useState({
        enterpriseName: '',
        commercialName: '',
        enterpriseLegalForm: '',
        mainActivity: '',
        streetNumber: '',
        streetName: '',
        postCode: '',
        city: '',
        enterpriseCapital: '',
        nominalValue: '',
        cashContribution: '',
        otherContribution: '',
        shareNumber: '',
        validatedKycAt: '',
        giveMoney: '',
        sourceMoney: '',
        profilClient: '',
        localisationClient: '',
        revenu: '',
        registerNumber: '',
        registerCountry: '',
        registerAuthority: '',
        registerDate: '',
        registerLink: '',
        statusFATCA: '',
        typeFATCA: '',
        GIIN: '',
        statusCRS: '',
        typeCRS: '',
        fiscalityIdentification: '',
        riskLevel: '',
        notesKBIS: '',
        notesStatus: '',
        notesAproval: '',
        notesHome: '',
        notesFinalcialState: '',
        notesAutocertification: '',
        notesFATCA: ''
    });

    useEffect(() => {
        setValues(company);
    }, [company])

    const handleChange = name => event => {
        const update = async () => {
            const data = { ...values, [name]: event.target.value };
            const token = getToken();
            await Axios.put(
                `${process.env.REACT_APP_API_URL}/kybcsventerprises/update`, { ...company, ...data }, {
                headers: {
                    'Authorization': `Bearer ${token}`,
                }
            }
            );
        };
        setValues({ ...values, [name]: event.target.value });
        update();
    };

    return (
        <form className={classes.container} noValidate autoComplete="off">
            <h4>ENTREE EN RELATION D'AFFAIRES</h4>
            <TextField
                id="company-enterpriseCapital"
                label="Capital de l'entreprise"
                className={classes.textField}
                value={values.enterpriseCapital}
                onChange={handleChange('enterpriseCapital')}
                margin="normal"
            />
            <TextField
                id="company-nominalValue"
                label="Valeur nominale des actions"
                className={classes.textField}
                value={values.nominalValue}
                onChange={handleChange('nominalValue')}
                margin="normal"
            />
            <TextField
                id="company-cashContribution"
                label="Contributaion en cash"
                className={classes.textField}
                value={values.cashContribution}
                onChange={handleChange('cashContribution')}
                margin="normal"
            />
            <TextField
                id="company-otherContribution"
                label="Autre contribution"
                className={classes.textField}
                value={values.otherContribution}
                onChange={handleChange('otherContribution')}
                margin="normal"
            />
            <TextField
                id="company-shareNumber"
                label="Nombre total de parts"
                className={classes.textField}
                value={values.shareNumber}
                onChange={handleChange('shareNumber')}
                margin="normal"
            />
            <TextField
                id="company-validatedKycAt"
                label="Date de validation du KYC"
                className={classes.textField}
                value={values.validatedKycAt}
                onChange={handleChange('validatedKycAt')}
                margin="normal"
            />

            <TextField
                id="company-sourceMoney"
                label="Source/destination des fonds"
                className={classes.textField}
                value={values.sourceMoney}
                onChange={handleChange('sourceMoney')}
                margin="normal"
            />

            <h4>IDENTIFICATION DE L'ENTITE</h4>

            <TextField
                id="company-enterpriseName"
                label="Dénomination sociale"
                className={classes.textField}
                value={values.enterpriseName}
                onChange={handleChange('enterpriseName')}
                margin="normal"
            />
            <TextField
                id="company-commercialName"
                label="Dénomination commerciale"
                className={classes.textField}
                value={values.commercialName}
                onChange={handleChange('commercialName')}
                margin="normal"
            />
            <TextField
                id="company-streetNumber"
                label="N°"
                className={classes.textField}
                value={values.streetNumber}
                onChange={handleChange('streetNumber')}
                margin="normal"
            />
            <TextField
                id="company-streetName"
                label="Adresse du siège social"
                className={classes.textField}
                value={values.streetName}
                onChange={handleChange('streetName')}
                margin="normal"
            />
            <TextField
                id="company-postCode"
                label="Code postal"
                className={classes.textField}
                value={values.postCode}
                onChange={handleChange('postCode')}
                margin="normal"
            />
            <TextField
                id="company-city"
                label="Ville"
                className={classes.textField}
                value={values.city}
                onChange={handleChange('city')}
                margin="normal"
            />
            <TextField
                id="company-enterpriseLegalForm"
                label="Forme juridique"
                className={classes.textField}
                value={values.enterpriseLegalForm}
                onChange={handleChange('enterpriseLegalForm')}
                margin="normal"
            />
            <TextField
                id="company-mainActivity"
                label="Type de produits/services"
                className={classes.textField}
                value={values.mainActivity}
                onChange={handleChange('mainActivity')}
                margin="normal"
                multiline="true"
            />
            <TextField
                id="company-profilClient"
                label="Profil de la clientèle"
                className={classes.textField}
                value={values.profilClient}
                onChange={handleChange('profilClient')}
                margin="normal"
            />
            <TextField
                id="company-localisationClient"
                label="Localisation de la clientèle"
                className={classes.textField}
                value={values.localisationClient}
                onChange={handleChange('localisationClient')}
                margin="normal"
            />
            <TextField
                id="company-revenu"
                label="Revenus connus ou déclarés"
                className={classes.textField}
                value={values.revenu}
                onChange={handleChange('revenu')}
                margin="normal"
            />
            <TextField
                id="company-registerNumber"
                label="Numéro d'enregistrement"
                className={classes.textField}
                value={values.registerNumber}
                onChange={handleChange('registerNumber')}
                margin="normal"
            />
            <TextField
                id="company-registerCountry"
                label="Pays d'enregistrement"
                className={classes.textField}
                value={values.registerCountry}
                onChange={handleChange('registerCountry')}
                margin="normal"
            />
            <TextField
                id="company-registerAuthority"
                label="Autorité d'enregistrement"
                className={classes.textField}
                value={values.registerAuthority}
                onChange={handleChange('registerAuthority')}
                margin="normal"
            />
            <TextField
                id="company-registerDate"
                label="Date d'enregistrement"
                className={classes.textField}
                value={values.registerDate}
                onChange={handleChange('registerDate')}
                margin="normal"
            />
            <TextField
                id="company-registerLink"
                label="Lien vers le site internet de l'autorité d'enregistrement"
                className={classes.textField}
                value={values.registerLink}
                onChange={handleChange('registerLink')}
                margin="normal"
            />

            <h4>FISCALITE FATCA</h4>

            <TextField
                id="company-statusFATCA"
                label="statusFATCA"
                className={classes.textField}
                value={values.statusFATCA}
                onChange={handleChange('statusFATCA')}
                margin="normal"
            />
            <TextField
                id="company-typeFATCA"
                label="typeFATCA"
                className={classes.textField}
                value={values.typeFATCA}
                onChange={handleChange('typeFATCA')}
                margin="normal"
            />
            <TextField
                id="company-GIIN"
                label="GIIN"
                className={classes.textField}
                value={values.GIIN}
                onChange={handleChange('GIIN')}
                margin="normal"
            />

            <h4>FISCALITE CRS</h4>

            <TextField
                id="company-statusCRS"
                label="statusCRS"
                className={classes.textField}
                value={values.statusCRS}
                onChange={handleChange('statusCRS')}
                margin="normal"
            />
            <TextField
                id="company-typeCRS"
                label="typeCRS"
                className={classes.textField}
                value={values.typeCRS}
                onChange={handleChange('typeCRS')}
                margin="normal"
            />
            <TextField
                id="company-fiscalityIdentification"
                label="fiscalityIdentification"
                className={classes.textField}
                value={values.fiscalityIdentification}
                onChange={handleChange('fiscalityIdentification')}
                margin="normal"
            />

            <h4>RISK AML CFT</h4>
            <TextField
                id="company-riskLevel"
                label="riskLevel"
                className={classes.textField}
                value={values.riskLevel}
                onChange={handleChange('riskLevel')}
                margin="normal"
            />
        </form >
    );
}

export default Company;