import React, { Fragment } from 'react';
import { IconButton } from '@material-ui/core';
import CachedIcon from '@material-ui/icons/Cached';

const Img = ({ match }) => {
    const [rotate, setRotate] = React.useState(0);

    const onClickRotate = () => {
        setRotate(rotate + 90);
    }

    return (
        <Fragment>
            <IconButton onClick={onClickRotate}><CachedIcon /></IconButton>
            <br />
            <img style={{ transform: `rotate(${rotate}deg)` }} src={`${process.env.REACT_APP_API_URL}/kybdocs/${match.params.projectId}/${match.params.filename}/display`} alt='img' />
        </Fragment>)
}

export default Img; 