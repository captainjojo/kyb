import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Axios from 'axios';
import { getToken } from '../adalConfig';

const useStyles = makeStyles(theme => ({
    container: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    textField: {
        marginLeft: theme.spacing(1),
        marginRight: theme.spacing(1),
        width: 400,
    },
    dense: {
        marginTop: 19,
    },
    menu: {
        width: 200,
    },
}));

const Actionnaire = ({ actionnaire }) => {
    const classes = useStyles();
    const [values, setValues] = useState({
        mandateType: '',
        email: '',
        associateName: '',
        associateFirstname: '',
        mainActivity: '',
        streetNumberAndName: '',
        postCode: '',
        city: '',
        residenceCountry: '',
        associateNationality: '',
        associatePhonenumber: '',
        birthCity: '',
        birthPostalcode: '',
        birthCountry: '',
        finantialContribution: '',
        birthday: '',
        screeningAML: '',
        actionnarialStructure: '',
        oparationNotice: '',
        noteIdentity: '',
        notesHome: '',
        notesNomination: '',
        notesProcuration: '',
        notesPublication: '',
        notesIBAN: '',
    });

    useEffect(() => {
        setValues(actionnaire);
    }, [actionnaire])

    const handleChange = name => event => {
        const update = async () => {
            const data = { ...values, [name]: event.target.value };
            const token = getToken();
            await Axios.put(
                `${process.env.REACT_APP_API_URL}/kybcsvactionnaires/update`, { ...actionnaire, ...data }, {
                headers: {
                    'Authorization': `Bearer ${token}`,
                }
            }
            );
        };
        setValues({ ...values, [name]: event.target.value });
        update();
    };



    return (
        <form className={classes.container} noValidate autoComplete="off">
            <TextField
                id="standard-mandateType"
                label="Civilité"
                className={classes.textField}
                value={values.mandateType}
                onChange={handleChange('mandateType')}
                margin="normal"
            />
            <TextField
                id="standard-email"
                label="Email"
                className={classes.textField}
                value={values.email}
                onChange={handleChange('email')}
                margin="normal"
            />
            <TextField
                id="standard-associateName"
                label="Nom"
                className={classes.textField}
                value={values.associateName}
                onChange={handleChange('associateName')}
                margin="normal"
            />
            <TextField
                id="standard-associateFirstname"
                label="Prénom"
                className={classes.textField}
                value={values.associateFirstname}
                onChange={handleChange('associateFirstname')}
                margin="normal"
            />

            <TextField
                id="standard-streetNumberAndName"
                label="Adresse"
                className={classes.textField}
                value={values.streetNumberAndName}
                onChange={handleChange('streetNumberAndName')}
                margin="normal"
            />
            <TextField
                id="standard-postCode"
                label="Code postal"
                className={classes.textField}
                value={values.postCode}
                onChange={handleChange('postCode')}
                margin="normal"
            />
            <TextField
                id="standard-city"
                label="Ville"
                className={classes.textField}
                value={values.city}
                onChange={handleChange('city')}
                margin="normal"
            />
            <TextField
                id="standard-residenceCountry"
                label="Pays de résidence"
                className={classes.textField}
                value={values.residenceCountry}
                onChange={handleChange('residenceCountry')}
                margin="normal"
            />
            <TextField
                id="standard-associateNationality"
                label="Nationalité"
                className={classes.textField}
                value={values.associateNationality}
                onChange={handleChange('associateNationality')}
                margin="normal"
            />
            <TextField
                id="standard-associatePhonenumber"
                label="Numéro de téléphone"
                className={classes.textField}
                value={values.associatePhonenumber}
                onChange={handleChange('associatePhonenumber')}
                margin="normal"
            />

            <TextField
                id="standard-birthday"
                label="Date de naissance"
                className={classes.textField}
                value={values.birthday}
                onChange={handleChange('birthday')}
                margin="normal"
            />

            <TextField
                id="standard-birthCity"
                label="Ville de naissance"
                className={classes.textField}
                value={values.birthCity}
                onChange={handleChange('birthCity')}
                margin="normal"
            />
            <TextField
                id="standard-birthPostalcode"
                label="Code postal de naissance"
                className={classes.textField}
                value={values.birthPostalcode}
                onChange={handleChange('birthPostalcode')}
                margin="normal"
            />
            <TextField
                id="standard-birthCountry"
                label="Pays de naissance"
                className={classes.textField}
                value={values.birthCountry}
                onChange={handleChange('birthCountry')}
                margin="normal"
            />
            <TextField
                id="standard-finantialContribution"
                label="Apports"
                className={classes.textField}
                value={values.finantialContribution}
                onChange={handleChange('finantialContribution')}
                margin="normal"
            />

            <TextField
                id="standard-screeningAML"
                label="Résultat Screening AML"
                className={classes.textField}
                value={values.screeningAML}
                onChange={handleChange('screeningAML')}
                margin="normal"
            />
        </form>
    );
}

export default Actionnaire;