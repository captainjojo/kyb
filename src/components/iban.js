import React, { useState } from 'react';
import { FormControl, Button, makeStyles } from '@material-ui/core';
import Axios from 'axios';
import TextField from '@material-ui/core/TextField';
import { getToken } from '../adalConfig';

const useStyles = makeStyles(theme => ({
    container: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    textField: {
        marginLeft: theme.spacing(1),
        marginRight: theme.spacing(1),
        width: 400,
    },
    dense: {
        marginTop: 19,
    },
    menu: {
        width: 200,
    },
}));

const Iban = ({ projectId }) => {
    const classes = useStyles();
    const [iban, setIban] = useState();

    const handleSubmit = async (evt) => {
        evt.preventDefault();
        const sendEmail = async (data) => {
            const token = getToken();
            await Axios.post(
                `${process.env.REACT_APP_API_URL}/tiime/sendMail`, { ...data }, {
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                }
            }
            );
        };

        sendEmail({ projectId, iban });
    }

    return (
        <form onSubmit={handleSubmit}>
            <FormControl>
                <TextField
                    id="standard-iban"
                    label="iban"
                    className={classes.textField}
                    onChange={e => setIban(e.target.value)}
                    margin="normal"
                />

            </FormControl>
            <Button variant="contained" color="primary" type="submit">Envoyer</Button>
        </form>
    )
}

export default Iban; 