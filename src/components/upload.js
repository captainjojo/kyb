import React, { useState } from 'react';
import { Input, Paper, FormLabel, FormControl, List, ListItem, Button } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import Axios from 'axios';
import { getToken } from '../adalConfig';

const useStyles = makeStyles(theme => ({
    root: {
        flexGrow: 1,
    },
    paper: {
        padding: theme.spacing(2),
        textAlign: 'left',
        color: theme.palette.text.secondary,
    },
}));

const Upload = ({ history }) => {
    const classes = useStyles();
    const [exl1, setExl1] = useState([]);
    const [exl2, setExl2] = useState([]);
    const [zip, setZip] = useState([]);
    const [json, setJson] = useState([]);

    const handleSubmit = async (evt) => {
        evt.preventDefault();
        const token = getToken();

        if (json.length !== 0) {
            const data = new FormData()
            json.forEach((file, i) => {
                data.append(i, file)
            })

            await Axios.post(
                `${process.env.REACT_APP_API_URL}/shine/upload`, data, { 'headers': { 'Content-Type': 'multipart/form-data', 'Authorization': `Bearer ${token}` } }
            );
        }

        if (exl1.length !== 0) {
            const data = new FormData()
            exl1.forEach((file, i) => {
                data.append(i, file)
            })

            await Axios.post(
                `${process.env.REACT_APP_API_URL}/kybcsventerprises/upload`, data, { 'headers': { 'Content-Type': 'multipart/form-data', 'Authorization': `Bearer ${token}` } }
            );
        }

        if (exl2.length !== 0) {
            const data2 = new FormData()
            exl2.forEach((file, i) => {
                data2.append(i, file)
            })
            await Axios.post(
                `${process.env.REACT_APP_API_URL}/kybcsvactionnaires/upload`, data2, { 'headers': { 'Content-Type': 'multipart/form-data', 'Authorization': `Bearer ${token}` } }
            );
        }

        if (zip.length !== 0) {
            const data3 = new FormData()
            zip.forEach((file, i) => {
                data3.append(i, file)
            })
            await Axios.post(
                `${process.env.REACT_APP_API_URL}/kybdocs/upload`, data3, { 'headers': { 'Content-Type': 'multipart/form-data', 'Authorization': `Bearer ${token}` } }
            );

            await Axios.post(`${process.env.REACT_APP_API_URL}/kybdocs/unzip`, {
                "source": `./upload/zip/${zip[0]['name']}`
            });
        }

        history.push(`/`);
    }

    return (
        <Paper className={classes.paper}>
            <form onSubmit={handleSubmit}>
                <List className={classes.root}>
                    <ListItem >
                        <FormControl className={classes.formControl}>
                            <FormLabel htmlFor="component-simple">Shine JSON </FormLabel>
                            <Input type="file" id="json" onChange={e => setJson(Array.from(e.target.files))} />
                        </FormControl>
                    </ListItem >
                    <ListItem >
                        <FormControl className={classes.formControl}>
                            <FormLabel htmlFor="component-simple">Entreprise CSV </FormLabel>
                            <Input type="file" id="exl1" onChange={e => setExl1(Array.from(e.target.files))} />
                        </FormControl>
                    </ListItem >
                    <ListItem >
                        <FormControl className={classes.formControl}>
                            <FormLabel htmlFor="component-simple">Actionnaire CSV</FormLabel>
                            <Input type="file" onChange={e => setExl2(Array.from(e.target.files))} />
                        </FormControl>
                    </ListItem>
                    <ListItem >
                        <FormControl className={classes.formControl}>
                            <FormLabel htmlFor="component-simple">Zip (utilisé le même nom que la société)</FormLabel>
                            <Input type="file" onChange={e => setZip(Array.from(e.target.files))} />
                        </FormControl>
                    </ListItem >
                </List>
                <Button variant="contained" color="primary" type="submit">Upload</Button>
            </form>
        </Paper>
    );
};

export default Upload;