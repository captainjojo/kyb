import React, { Fragment, useState, useEffect } from 'react';
import CheckIcon from '@material-ui/icons/Check';
import ClearIcon from '@material-ui/icons/Clear';
import Axios from 'axios';
import { getToken } from '../adalConfig';

const PdfStatus = ({ path, status }) => {
    const [statusFile, setStatusFile] = useState('');

    useEffect(() => {
        setStatusFile(status);
    }, [status])

    const save = (status) => {
        const update = async (status) => {
            const token = getToken();
            await Axios.put(
                `${process.env.REACT_APP_API_URL}/kybdocs/update`, { path, status }, {
                headers: {
                    "Authorization": `Bearer ${token}`,
                }
            }
            );
        };
        update(status);
        setStatusFile(status);
    }

    if (statusFile === 'validated') {
        return (<CheckIcon htmlColor={'green'}></CheckIcon>);
    }

    if (statusFile === 'unvalidated') {
        return (<ClearIcon htmlColor={'red'}></ClearIcon>);
    }

    return (<Fragment><CheckIcon onClick={save.bind(this, 'validated')}></CheckIcon> <ClearIcon onClick={save.bind(this, 'unvalidated')}></ClearIcon></Fragment>);
}


export default PdfStatus;