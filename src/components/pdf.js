
import React, { Fragment, useEffect } from 'react';
import { pdfjs, Document, Page } from 'react-pdf';
import KeyboardArrowLeftIcon from '@material-ui/icons/KeyboardArrowLeft';
import KeyboardArrowRightIcon from '@material-ui/icons/KeyboardArrowRight';
import IconButton from '@material-ui/core/IconButton';
import ZoomInIcon from '@material-ui/icons/ZoomIn';
import ZoomOutIcon from '@material-ui/icons/ZoomOut';
import CachedIcon from '@material-ui/icons/Cached';

pdfjs.GlobalWorkerOptions.workerSrc = `//cdnjs.cloudflare.com/ajax/libs/pdf.js/${pdfjs.version}/pdf.worker.js`;

const Pdf = ({ match }) => {
    const [numPages, setNumPages] = React.useState(0);
    const [scale, setScale] = React.useState(1);
    const [pageNumber, setPageNumber] = React.useState(1);
    const [pdf, setPdf] = React.useState();
    const [rotate, setRotate] = React.useState(0);

    const onClickZoomOut = () => {
        setScale(scale - 1);
    }

    const onClickZoomIn = () => {
        setScale(scale + 1);
    }

    const onClickLeft = () => {
        setPageNumber(pageNumber - 1);
    }

    const onClickRotate = () => {
        setRotate(rotate + 90);
    }

    const onClickRight = () => {
        setPageNumber(pageNumber + 1);
    }

    const onDocumentLoadSuccess = ({ numPages }) => {
        setNumPages(numPages);
    }

    useEffect(() => {
        setPdf(`${process.env.REACT_APP_API_URL}/kybdocs/${match.params.projectId}/${match.params.filename}/display`);
    }, [match]);

    return (
        <Fragment>
            <IconButton onClick={onClickZoomIn}><ZoomInIcon /></IconButton>
            <IconButton onClick={onClickZoomOut}><ZoomOutIcon /></IconButton>
            <IconButton onClick={onClickRotate}><CachedIcon /></IconButton>
            <Document
                file={pdf}
                onLoadSuccess={onDocumentLoadSuccess}
                rotate={rotate}
            >
                <Page pageNumber={pageNumber} scale={scale} />
            </Document>
            <IconButton onClick={onClickLeft}><KeyboardArrowLeftIcon /></IconButton><span>Page {pageNumber} of {numPages}</span><IconButton onClick={onClickRight}><KeyboardArrowRightIcon /></IconButton>
        </Fragment>
    )

};

export default Pdf;