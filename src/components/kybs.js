import React, { Fragment, useEffect, useState } from 'react';
import { Link } from "react-router-dom";
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import { Button } from '@material-ui/core';
import Axios from 'axios';
import SearchIcon from '@material-ui/icons/Search';
import InputBase from '@material-ui/core/InputBase';
import IconButton from '@material-ui/core/IconButton';
import { getToken } from '../adalConfig';

const useStyles = makeStyles(theme => ({
    root: {
        width: '100%',
        marginTop: theme.spacing(3),
        overflowX: 'auto',
    },
    table: {
        minWidth: 650,
    },
    button: {
        margin: theme.spacing(1),
        color: "white",
    },
    color: {
        color: "white",
        textDecoration: "none",

    }
}));

const Kybs = () => {
    const classes = useStyles();
    const [kybs, setKybs] = useState([]);

    useEffect(() => {
        const fetchData = async () => {
            const token = getToken();
            const result = await Axios(
                `${process.env.REACT_APP_API_URL}/kybcsventerprises`, {
                headers: {
                    "Authorization": `Bearer ${token}`,
                }
            }
            );
            setKybs(result.data);
        };
        fetchData();
    }, []);

    const search = (event) => {
        const fetchSearch = async (projectNom) => {
            const token = getToken();
            const result = await Axios(
                `${process.env.REACT_APP_API_URL}/kybcsventerprises/search?projectName=${projectNom}`, {
                headers: {
                    'Authorization': `Bearer ${token}`,
                }
            }

            );
            setKybs(result.data);
        };
        fetchSearch(event.target.value);
    }

    return (
        <Fragment>
            <InputBase
                className={classes.input}
                placeholder="Recherche un projet"
                onChange={search}
            />
            <IconButton className={classes.iconButton} aria-label="search">
                <SearchIcon />
            </IconButton>
            <Table className={classes.table}>
                <TableHead>
                    <TableRow>
                        <TableCell>Id Projet</TableCell>
                        <TableCell>Nom du projet</TableCell>
                        <TableCell align="left">Nom de l'entreprise</TableCell>
                        <TableCell align="left">Nom commercial</TableCell>
                        <TableCell align="left">Forme legal</TableCell>
                        <TableCell align="left">Statut</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {kybs.map(row => (
                        <TableRow key={row.projectId}>
                            <TableCell component="th" scope="row">
                                <Link to={`/kyb/${row.projectId}`}>KYC {row.projectId}</Link>
                            </TableCell>
                            <TableCell align="left">{row.projectNom}</TableCell>
                            <TableCell align="left">{row.enterpriseName}</TableCell>
                            <TableCell align="left">{row.commercialName}</TableCell>
                            <TableCell align="left">{row.enterpriseLegalForm}</TableCell>
                            <TableCell align="left">{row.status}</TableCell>
                        </TableRow>
                    ))}
                </TableBody>
            </Table>
            <Button variant="contained" color="primary" className={classes.button}>
                <Link to='/upload' className={classes.color}>Nouveau KYB</Link>
            </Button>
        </Fragment>
    );
};

export default Kybs;