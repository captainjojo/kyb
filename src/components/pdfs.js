import React, { useEffect, useState } from 'react';
import { Link } from "react-router-dom";
import Pdf from './pdf';
import Img from './img';
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Axios from 'axios';
import PdfStatus from './pdfStatus';
import PdfType from './pdfType';
import { List, ListItem } from '@material-ui/core';
import { getToken } from '../adalConfig';

const Pdfs = ({ id }) => {

    const [pdfs, setPdfs] = useState([]);

    useEffect(() => {
        const fetchData = async (projectId) => {
            const token = getToken();
            const result = await Axios(
                `${process.env.REACT_APP_API_URL}/kybdocs/${projectId}/search`, {
                headers: {
                    "Authorization": `Bearer ${token}`,
                }
            }
            );
            setPdfs(result.data);
        };


        fetchData(id);
    }, [id]);


    return (
        <Router>
            <List>
                {pdfs.map((item, index) => (
                    <ListItem key={index}>
                        <Link to={`/${item.ext}/${item.filename}/${id}`}>{item.filename}</Link>
                        <PdfStatus path={'upload/kybdocs/' + id + '/' + item.filename} status={item.status}></PdfStatus>
                        <PdfType file={item} id={id}></PdfType>
                    </ListItem>
                ))
                }

            </List>
            <Switch>
                <Route path="/pdf/:filename/:projectId" component={Pdf} />
                <Route path="/jpg/:filename/:projectId" component={Img} />
            </Switch>
        </Router >
    );
};

export default Pdfs;