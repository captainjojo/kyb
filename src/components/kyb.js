import React, { useState, useEffect, Fragment } from 'react';
import Axios from 'axios';
import { makeStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List'
import ListSubheader from '@material-ui/core/ListSubheader';
import { Divider, Link, Grid, Select, MenuItem, FormControl } from '@material-ui/core';
import Actionnaire from './actionnaire';
import Company from './company';
import Iban from './iban';
import { getToken } from '../adalConfig';

const useStyles = makeStyles(theme => ({
    root: {
        width: '100%',
        maxWidth: 500,
        backgroundColor: theme.palette.background.paper,
        position: 'relative',
        overflow: 'auto',
    },
    listSection: {
        backgroundColor: 'inherit',
    },
    ul: {
        backgroundColor: 'inherit',
        padding: 0,
    },
    textField: {
        marginLeft: theme.spacing(1),
        marginRight: theme.spacing(1),
    },
    formControl: {
        margin: theme.spacing(1),
        maxWidth: 180,
    },
    hidden: {
        display: "none"
    }
}));

const Kyb = ({ id }) => {
    const [kyb, setKyb] = useState(0);
    const [kycs, setKycs] = useState([]);
    const [display, setDisplay] = useState('societe');
    const [status, setStatus] = useState('');
    const [accredidator, setAccredidator] = useState('');

    const onClickList = (value) => {

        setDisplay(value);
    }



    useEffect(() => {
        const token = getToken();
        const fetchData = async (projectId) => {
            const result = await Axios(
                `${process.env.REACT_APP_API_URL}/kybcsventerprises/${projectId}/search`, {
                headers: {
                    'Authorization': `Bearer ${token}`,
                }
            }
            );
            setKyb(result.data[0]);
            setStatus(result.data[0].status);
            setAccredidator(result.data[0].accredidator);
        };

        const fetchActionnaires = async (projectId) => {
            const result = await Axios(
                `${process.env.REACT_APP_API_URL}/kybcsvactionnaires/${projectId}/projectshareholders`, {
                headers: {
                    'Authorization': `Bearer ${token}`,
                }
            }
            );
            setKycs(result.data);

        };


        fetchData(id);
        fetchActionnaires(id);

    }, [id]);

    const handleChange = (event) => {
        const token = getToken();
        const update = async (status) => {
            await Axios.put(
                `${process.env.REACT_APP_API_URL}/kybcsventerprises/update`, { ...kyb, status }, {
                headers: {
                    'Authorization': `Bearer ${token}`,
                }
            }
            );
        };

        setStatus(event.target.value);
        update(event.target.value);
    };

    const handleChangeAccredidator = (event) => {
        const token = getToken();
        const update = async (accredidator) => {
            await Axios.put(
                `${process.env.REACT_APP_API_URL}/kybcsventerprises/update`, { ...kyb, accredidator }, {
                headers: {
                    'Authorization': `Bearer ${token}`,
                }
            }
            );
        };

        setAccredidator(event.target.value);
        update(event.target.value);
    };

    const classes = useStyles();
    const dense = true;
    return (
        <Fragment>
            <Grid container>
                <Grid item lg={3}>
                    <List>
                        <Link onClick={onClickList.bind(this, 'societe')} value="societe">Société</Link>
                        <br />
                        {kycs.map((row, index) => (
                            <Fragment key={row.projectId}>
                                <Link onClick={onClickList.bind(this, 'actionnaire' + index)} >Actionnaire {index + 1}</Link>
                                <br />
                            </Fragment>
                        ))}
                    </List>
                </Grid>
                <Grid item lg={4}>
                    <FormControl className={classes.formControl}>
                        <Select
                            value={accredidator}
                            onChange={handleChangeAccredidator}
                            inputProps={{
                                name: 'statut',
                                id: 'statut-helper',
                            }}
                        >
                            <MenuItem value="nono">
                                <em>NONE</em>
                            </MenuItem>
                            <MenuItem value='Acreditateur 1'>Acreditateur 1</MenuItem>
                            <MenuItem value='Acreditateur 2'>Acreditateur 2</MenuItem>
                            <MenuItem value='Acreditateur 3'>Acreditateur 3</MenuItem>
                        </Select>
                    </FormControl>
                </Grid>
                <Grid item lg={4}>
                    <FormControl className={classes.formControl}>
                        <Select
                            value={status}
                            onChange={handleChange}
                            inputProps={{
                                name: 'statut',
                                id: 'statut-helper',
                            }}
                        >
                            <MenuItem value="New">
                                <em>New</em>
                            </MenuItem>
                            <MenuItem value='KYC 1'>KYC 1 - en cours</MenuItem>
                            <MenuItem value='KYC 1-relance'>KYC 1 - relance partenaire</MenuItem>
                            <MenuItem value='KYC 2'>KYC 2 - en cours</MenuItem>
                            <MenuItem value='KYC 2-relance'>KYC 2 - relance partenaire</MenuItem>
                            <MenuItem value='Accepté'>Accepté</MenuItem>
                            <MenuItem value='refus-Kyc1'>Refus - KYC 1</MenuItem>
                            <MenuItem value='refus-Kyc2'>Refus - KYC 2</MenuItem>
                            <MenuItem value='IBAN-creer'>IBAN - Créé et transmis</MenuItem>
                            <MenuItem value='Virement dépot - Analyse reception des fonds en cours'>Virement dépot - Analyse reception des fonds en cours</MenuItem>
                            <MenuItem value='Virement dépot - Relance client'>Virement dépot - Relance client</MenuItem>
                            <MenuItem value='Attestation transmise'>Attestation transmise</MenuItem>
                            <MenuItem value='Libération des fonds - demande client'>Libération des fonds - demande client</MenuItem>
                            <MenuItem value='Libération des fonds - relance client'>Libération des fonds - relance client</MenuItem>
                            <MenuItem value='Libération des fonds - virement emit'>Libération des fonds - virement emit</MenuItem>
                            <MenuItem value='Libération des fonds - reception client confirmé'>Libération des fonds - reception client confirmé</MenuItem>
                            <MenuItem value='Compte - clôturé'>Compte - clôturé</MenuItem>
                        </Select>
                    </FormControl>
                </Grid>
            </Grid>
            <Divider />
            <List className={classes.root} subheader={<li />} dense={dense}>
                <li className={classes.listSection}>
                    <ul className={classes.ul}>
                        {kycs.map((row, index) => {
                            return (display === 'actionnaire' + index ? (<Fragment key={row.projectId} >
                                <ListSubheader>Actionnaire</ListSubheader>
                                <Actionnaire actionnaire={row}></Actionnaire>
                            </Fragment>
                            ) : (<Fragment key={row.projectId} ></Fragment>));
                        })}
                        {display === 'societe' ? (<Fragment>
                            <ListSubheader>Société</ListSubheader>
                            <Company company={kyb}></Company>
                        </Fragment>) : (<br />)}
                    </ul>
                </li>

            </List >
            <Divider />
            <h4>ENVOIE IBAN</h4>
            <Iban projectId={kyb.projectId}></Iban>

        </Fragment >
    );
};

export default Kyb;