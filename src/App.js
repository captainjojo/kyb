
import './App.css';
import React from 'react';
import { BrowserRouter as Router, Switch, Route, NavLink } from "react-router-dom";
import Kybs from './components/kybs';
import Upload from './components/upload';
import KybContainer from './components/KybContainer';
import { Container, AppBar, MenuItem } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import KeyboardArrowLeftIcon from '@material-ui/icons/KeyboardArrowLeft';
import { MuiPickersUtilsProvider } from '@material-ui/pickers';
import DateFnsUtils from '@date-io/date-fns';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import { authContext } from './adalConfig';

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
  },
  container: {
    marginTop: theme.spacing(2),
    maxWidth: 1680,
  },
  title: {
    flexGrow: 1,
  },
  color: {
    color: "white",
  }
}));


export default function App() {
  const classes = useStyles();


  const logout = () => {
    authContext.logOut();
  }

  return (
    <MuiPickersUtilsProvider utils={DateFnsUtils}>
      <Router>

        <AppBar position="static">
          <Toolbar>
            <NavLink to="/" activeClassName={classes.color}>
              <MenuItem>
                <IconButton color="inherit">
                  <KeyboardArrowLeftIcon />
                </IconButton>
              </MenuItem>
            </NavLink>
            <Typography variant="h6" className={classes.title}>
              Dashboard
          </Typography>
            <IconButton color="inherit">
              <ExitToAppIcon onClick={logout} />
            </IconButton>
          </Toolbar>
        </AppBar>
        <Container maxWidthLg="false" className={classes.container} >
          <Switch>
            <Route path="/kyb/:id" component={KybContainer} />
            <Route path="/upload" component={Upload}></Route>

            <Route path="/" component={Kybs} />

          </Switch>
        </Container>
      </Router>
    </MuiPickersUtilsProvider>
  );
}